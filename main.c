#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>

#define DHT22    
#define DHT_PORT        PORTD
#define DHT_DDR         DDRD
#define DHT_PIN         PIND
#define DHT_BIT         1

unsigned char portlcd = 0;

#define e1 I2C_SendByteByADDR(portlcd |= 0x04,0b01001110) // установка линии E в 1
#define e0 I2C_SendByteByADDR(portlcd &= ~0x04,0b01001110) // установка линии E в 0
#define rs1 I2C_SendByteByADDR(portlcd |= 0x01,0b01001110) // установка линии RS в 1
#define rs0 I2C_SendByteByADDR(portlcd &= ~0x01,0b01001110) // установка линии RS в 0
#define setled() I2C_SendByteByADDR(portlcd |= 0x08,0b01001110) // включение подсветки
#define setwrite() I2C_SendByteByADDR(portlcd &= ~0x02,0b01001110) // установка записи в память дисплея

volatile int timer = 0;
volatile int update_text = 0;
uint8_t data[5] = {0, 0, 0, 0, 0};


void I2C_Init (void) {
        TWBR = 0x20;
}

void I2C_StartCondition(void) {
        TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
        while (!(TWCR & (1 << TWINT)));//подождем пока установится TWIN
}

void I2C_StopCondition(void) {
        TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
}

void I2C_SendByte(unsigned char c) {
        TWDR = c;//запишем байт в регистр данных
        TWCR = (1 << TWINT) | (1 << TWEN);//включим передачу байта
        while (!(TWCR & (1 << TWINT)));//подождем пока установится TWIN
}

void I2C_SendByteByADDR(unsigned char c, unsigned char addr) {
        I2C_StartCondition(); // Отправим условие START
        I2C_SendByte(addr); // Отправим в шину адрес устройства + бит чтения-записи
        I2C_SendByte(c);// Отправим байт данных
        I2C_StopCondition();// Отправим условие STOP
}

unsigned char I2C_ReadByte(void) {
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
        while (!(TWCR & (1 << TWINT)));//ожидание установки бита TWIN
        return TWDR;//читаем регистр данных
}

unsigned char I2C_ReadLastByte(void) {
        TWCR = (1 << TWINT) | (1 << TWEN);
        while (!(TWCR & (1 << TWINT)));//ожидание установки бита TWIN
        return TWDR;//читаем регистр данных
}

void sendhalfbyte(unsigned char c) {
        c <<= 4;
        e1; //включаем линию Е
        _delay_us(50);

        I2C_SendByteByADDR(portlcd|c,0b01001110);
        e0; //выключаем линию Е
        _delay_us(50);
}

void sendbyte(unsigned char c, unsigned char mode) {
        if (mode==0)  
                rs0;
        else          
                rs1;
        unsigned char hc=0;
        hc = c >> 4;
        sendhalfbyte(hc);
        sendhalfbyte(c);
}

void sendcharlcd(unsigned char c) {
        sendbyte(c,1);
}

void setpos(unsigned char x, unsigned y) {
        switch(y) {
                case 0:
                        sendbyte(x | 0x80, 0);
                        break;
                case 1:
                        sendbyte((0x40 + x) | 0x80, 0);
                        break;
                case 2:
                        sendbyte((0x14 + x) | 0x80, 0);
                        break;
                case 3:
                        sendbyte((0x54 + x) | 0x80, 0);
                        break;
        }
}

void LCD_ini(void) {
        _delay_ms(15); 
        sendhalfbyte(0b00000011);
        _delay_ms(4);
        sendhalfbyte(0b00000011);
        _delay_us(100);
        sendhalfbyte(0b00000011);
        _delay_ms(1);
        sendhalfbyte(0b00000010);
        _delay_ms(1);
        sendbyte(0b00101000, 0); //4бит-режим (DL=0) и 2 линии (N=1)
        _delay_ms(1);
        sendbyte(0b00001100, 0); //включаем изображение на дисплее (D=1), курсоры никакие не включаем (C=0, B=0)
        _delay_ms(1);
        sendbyte(0b00000110, 0); //курсор (хоть он у нас и невидимый) будет двигаться влево
        _delay_ms(1);
        setled();  //подсветка
        setwrite();//запись
}

void clearlcd(void) {
        sendbyte(0b00000001, 0);
        _delay_us(1500);
}

void str_lcd (char str1[]) {
        wchar_t n;
        for(n = 0; str1[n] != '\0'; n++)
                sendcharlcd(str1[n]);
}

int read_dht_hum() {
        unsigned char i, j;

        DHT_DDR |= (1 << DHT_BIT); 
        DHT_PORT &= ~(1 << DHT_BIT);
        _delay_ms(18);

        DHT_PORT |= (1 << DHT_BIT);
        DHT_DDR &= ~(1 << DHT_BIT);
        _delay_us(50);

        if (DHT_PIN & (1<<DHT_BIT))
                return 0;
        _delay_us(80);

        if (!(DHT_PIN & (1 << DHT_BIT)))
                return 0;

        while (DHT_PIN & (1 << DHT_BIT));

        for (int j = 0; j < 5; j++) {
                data[j] = 0;
                for(int i = 0; i < 8; i++) {
                        while (!(DHT_PIN & (1 << DHT_BIT)));
                        _delay_us (30);

                        if (DHT_PIN & (1 << DHT_BIT))
                                data[j] |= 1 << (7 - i);
                        while (DHT_PIN & (1 << DHT_BIT));
                }
        }

        return 1;
}

float dht_read() {
        float hum = read_dht_hum();
        if(hum == 1) 
                return data[2] + 0.1 * data[3];
        else 
                return 0;
}

ISR(TIMER1_COMPA_vect) {
        timer ++;
}

ISR(INT0_vect) { 
        update_text = 1;
}

void setup() {
        cli();
        TCCR1A = 0;
        TCCR1B = 0;
        OCR1A = 15624;
        TCCR1B |= (1 << WGM12);
        TCCR1B |= (1 << CS10);
        TCCR1B |= (1 << CS12);
        TIMSK1 |= (1 << OCIE1A);

        DDRD  = 0b11110000;
        PORTD = 0b00000100;

        EICRA |= (1 << ISC01);
        EIMSK |= (1 << INT0);

        sei();

        I2C_Init();
        LCD_ini();
        clearlcd();
}


int main() {
        char buffer[64];
        setup();

        float temps[4]; 
        int times[4];
        int index = 0;

        while (1) {
                if (update_text) {
                        float temp = dht_read();
                        temps[index] = temp;
                        times[index] = timer;

                        clearlcd();
                        setpos(0, 0);
                        str_lcd("Temp:");
                        setpos(0, 2);
                        str_lcd("Time:");

                        for (int i = 0; i < 4; i ++) {
                                char char_temp[5];
                                int ivalue = temps[(i+index)%4];
                                char_temp[0] = (ivalue * 10) % 100;
                                char_temp[1] = ((ivalue * 10) - 100 * char_temp[0]) % 10;
                                char_temp[2] = '.';
                                char_temp[3] = (ivalue * 10) % 1;
                                char_temp[4] = '\0';

                                char* char_time;
                                itoa(times[(i+index)%4], char_time, 10);

                                setpos(i*5, 1);
                                str_lcd(char_temp);
                                setpos(i*5, 3);
                                str_lcd(char_time);
                        }

                        update_text = 0;
                        index = (index + 1) % 4;
                }
        }
        return 0;
}

